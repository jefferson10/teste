<?php

class HomeController extends BaseController {

    public $contabilizador;
    public $listaTelefonica;

    public function contabiliza() {


        $vetor = $_GET['vetor'];
        $qtd = $vetor[0];
        $this->listaTelefonica = HomeController::formataListaTelefonica($vetor[1]);
        if ($qtd != count($this->listaTelefonica)) {
            return "A quantidade informada, não corresponde com a real quantidade!";
        }
        $this->contabilizador = HomeController::criaArvores($this->listaTelefonica);
        $tamanho = HomeController::contabilizarEspacoGastoPelaListaTelefonica($this->contabilizador);
        return 'Espaço gasto para armazenar a lista de contatos:  '.$tamanho;
    }

    //metodo que formata a entrada da lista
    public static function formataListaTelefonica($lista) {
        //tira as letras da lista telefonica
        $listaSemLetras = preg_replace("/[^0-9 \n]/", "", $lista);
        //tira quebra de linha e espaço do começo e do fim da lista telefonica
        $listaSemEspacoNoInicioEfim = trim($listaSemLetras);
        //divide a lista telefonica em um array de numeros telefonico
        $listaPronta = explode("\n", $listaSemEspacoNoInicioEfim);
        return $listaPronta;
    }

    //cria as arvores para a lista telefonica
    public static function criaArvores($listaTelefonica) {
        $contabilizador = new ContabilizadorContatos();
        $contabilizador->criaArvores($listaTelefonica);
        return $contabilizador;
    }

    //contabiliza o espaço gasto pela lista telefônica
    public static function contabilizarEspacoGastoPelaListaTelefonica($contabilizador) {
        $tamanho = 0;
        //obtem as arvores
        $arvores = $contabilizador->arvores;
        //percorre as arvores
        for ($i = 0; $i < count($arvores); $i++) {
            //obtem a arvore corrente
            $arvore = $arvores[$i];
            //obtem a lista de nos
            $nos = $arvore->nos;
            //percorre os nos
            for ($t = 0; $t < count($nos); $t++) {
                //obtem a listaDeDadosCorrente para pegar seu tamanho
                $listaDeDadosCorrente = $nos[$t]->listaDeDadosNo;
                //incrementa o tamanho,usando o sizeof da listaDeDadosCorrente
                $tamanho = $tamanho + strlen(implode("", $listaDeDadosCorrente));
            }
        }
        return $tamanho;
    }

}

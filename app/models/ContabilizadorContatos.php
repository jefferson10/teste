<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConjuntoDeListas
 *
 * @author jefferson
 */
class ContabilizadorContatos {

    public $arvores;

    function __construct() {
        $this->arvores = array();
    }

    //metodo que recebe a lista telefônica e cria arvores para serem relacionadas
    function criaArvores($listaTelefonica) {

        for ($i = 0; $i < count($listaTelefonica); $i++) {
            //confere se já contém sequência para o número a ser testado
            $resultado = $this->confereSeExisteListaQueComecaComOPrimeiroCaractereDoNumeroTelefonico($listaTelefonica[$i]);
            //se $resultado >=0, é porque ja possui uma sequência, e o $resultado é o indice da arvore que a contém
            if ($resultado >= 0) {
                $this->arvores[$resultado]->insereTelefone($listaTelefonica[$i]);            
            } else {
                //se $resultado < 0 será necessário cria outra arvore, com uma nova sequência
                //cria uma arvore
                $arvore = new Arvore();
                //insere o numero de telefone corrente na arvore,formando uma nova sequência
                $arvore->insereNovoNoNaLista($listaTelefonica[$i]);
                //insere a arvore criada na lista de arvores
                $this->arvores[count($this->arvores)] = $arvore;
            }
        }
    }

    //metodo para conferir se já existe uma arvore que se inicia com o primeiro caractere do número telefônico
    function confereSeExisteListaQueComecaComOPrimeiroCaractereDoNumeroTelefonico($telefone) {
        $resultado = -1;
        //loop através das arvores
        for ($i = 0; $i < count($this->arvores); $i++) {
            //pega a arvore corrente
            $arvore = $this->arvores[$i];
            //pede a arvore corrente para verificar sem possui um inicio igual ao do telefone
            if ($arvore->confereComecoDaLista($telefone[0])) {
                //retorna o indice da arvore que contem o mesmo inicio
                return $i;
            }
        }
        //caso não exista nenhuma arvore com o mesmo inicio sera retornada -1
        return $resultado;
    }

}

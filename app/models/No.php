<?php

/**
 * Description of ItemLista
 *
 * @author jefferson
 */
class No {

    public $inicio;
    public $listaDeDadosNo;

    function __construct($inicio) {
        $this->inicio = $inicio;
        $this->listaDeDadosNo = array();
    }
    //cria a listaDeDadosNo com o inicioTelefon, e o telefone
    function criaListaDeDadosNo($inicioTelefone, $telefone) {
        //contador da nova listaDeDadosNo
        $contadorListaDeDadosNo = 0;
        //percorre o telefone
        for ($i = $inicioTelefone; $i < strlen($telefone); $i++) {
            //adiciona o caractere do telefone, a listaDeDadosNo 
            $this->listaDeDadosNo[$contadorListaDeDadosNo] = $telefone[$i];
            //incrementa o contador da nova listaDeDadosNo
            $contadorListaDeDadosNo++;
        }
    }

}

<?php

/**
 * Description of Lista
 *
 * @author jefferson
 */
class Arvore {

    public $nos;

    function __construct() {
        $this->nos = array();
    }

    //metodo que compara o primeiro caractere da $listaDeDadosNo, com o $primeiroAlgarismoTelefone 
    function confereComecoDaLista($primeiroAlgarismoTelefone) {
        //percorre os nos em busca da semelhança
        for ($i = 0; $i < count($this->nos); $i++) {
            //obtem o no corrente
            $no = $this->nos[$i];
            //obtem a listaDeDadosNo
            $listaDeDadosNo = $no->listaDeDadosNo;
            //obtem o inicio da listaDeDadosNo
            $inicio = $no->inicio;
            /* a condição será verdadeira quando o inicio é igual a 0 : ou seja,esta listaDeDadosNo
              é o começo de alguma sequência de numeros telefônicos
              os primeiros caracteres também devem ser iguais, para que o numero testado, pertença
              a essa sequência */
            if ($inicio == 0 && $listaDeDadosNo[0] == $primeiroAlgarismoTelefone) {
                return true;
            }
        }
        //caso o numero não pertença a essa sequência, será retornado false
        return false;
    }
    //metodo que insere o telefone na arvore, após ter achado uma sequência
    function insereTelefone($telefone) {
        //pecorre os nos da arvore
        for ($i = 0; $i < count($this->nos); $i++) {
            //obtem o no corrente
            $no = $this->nos[$i];
            //obtem a listaDeDadosNo do no corrente
            $listaDeDadosNo = $no->listaDeDadosNo;
            //percorre a listaDeDadosNo do no corrente
            for ($t = 0; $t < count($listaDeDadosNo); $t++) {
                /*faz uma comparação para que quando acabar de inserir o telefone
                ele não percorra mais a listaDeDadosNo*/
                if ($t < strlen($telefone - 1)) {
                    //comparação para ver se os caracteres são diferentes
                    if ($listaDeDadosNo[$t] != $telefone[$t]) {
                        //caso verdadeiro sera criado mais um no
                        $no = new No($t);
                        /*cria a listaDeDadosNo do no, com o resto do numero de telefone
                        que é diferente da sequência*/
                        $no->criaListaDeDadosNo($t, $telefone);
                        //adiciona o no, a lista de nos
                        $this->nos[count($this->nos)] = $no;
                        return 1;
                    }
                }else{
                    //para a execução,caso o telefone já tenha sido inserido completamente
                    break;  
                }
            }
        }
    }
    //metodo que insere um novo no na arvore,com o telefone corrente
    function insereNovoNoNaLista($telefone) {
        //cria um novo no
        $no = new No(0);
        /* cria a listaDeDadosNo do no, com o numero de telefone, formando uma 
          nova sequência */
        $no->criaListaDeDadosNo(0, $telefone);
        //insere o no na lista de nos
        $this->nos[count($this->nos)] = $no;
    }

}

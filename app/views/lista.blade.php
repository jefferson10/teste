<html lang="pt">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Lista Telefônica</title>      
        <link rel="stylesheet" type="text/css" href="{{url('/css/bootstrap.css')}}"/>        
        <link rel="stylesheet" type="text/css" href="{{url('/css/bootstrap.min.css')}}"/>        
        <script type="text/javascript" src="{{url('/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/knockout-3.1.0.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/knockout.validation.js')}}"></script>
        <script type="text/javascript" src="{{url('/js/funcoesAtualizar.js')}}"></script>
    </head>
    <body>


        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Contabilizador de Armazenamento de Contatos Telefônicos</a>
                </div>                
            </div>
        </nav>
        <p class="text-center"><h3>A Quantidade de telefones, corresponde ao total de telefones que serão inseridos do campo
            Lista Telefônica. Letras serão desconsideradas.</h3>
           </p>
           <br>
           <br>
           <br>
        <form class="form-horizontal" data-bind="submit:enviar">
            <fieldset>

                <!-- Form Name -->
                <legend>Inserir lista telefônica</legend>

                <!-- Text input-->

                <div class="form-group">                    
                    <label class="col-md-4 control-label" for="textinput">Quantidade de telefones</label>  
                    <div class="col-md-4">
                        <input id="textinput" name="textinput" type="text"  class="form-control input-md" data-bind="value:qtd"  required pattern="^[0-9].*">                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label" for="textarea" >Lista Telefônica</label>
                    <div class="col-md-4">                     
                        <textarea class="form-control" id="textarea" type="text" name="textarea" data-bind="value:lista" required pattern="^[0-9].*"></textarea>
                    </div>
                </div>


                <!-- Button (Double) -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="button1id"></label>
                    <div class="col-md-8">
                        <span class="help-block" data-bind="text:resultado"></span>  

                        <button id="button1id" name="button1id" class="btn btn-success" type="submit"> Enviar</button>
                        <button id="button2id" name="button2id" class="btn btn-danger" type="reset">Limpar</button>
                    </div>
                </div>
                
            </fieldset>
        </form>
        

    </body>
</html>


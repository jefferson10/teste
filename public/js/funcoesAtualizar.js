$(document).ready(function () {
    var AtualizaModel = function () {
        var self = this;

        self.qtd = ko.observable("");
        self.lista = ko.observable("");
        self.resultado = ko.observable("");

        self.enviar = function () {

            var vetor = [self.qtd(), self.lista()];
            $.ajax({
                async: false,
                type: "GET",
                url: "http://localhost/teste/public/contabiliza",
                data: {
                    'vetor': vetor
                },
                success: function (data) {
                    self.resultado(data);
                },
                error: function (error) {
                    alert("Erro na requisição!");
                }
            });
        };

    };


    ko.validation.init();
    ko.applyBindings(new AtualizaModel());
});